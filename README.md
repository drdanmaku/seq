Seq
===
Seq is an experimental virtual machine and development environment.

Installation
------------
```
go get -u -v github.com/drdanmaku/seq
go install -v github.com/drdanmaku/seq/cmd/iseq
iseq
```

You can also use `iseq` from within Emacs with `M-x inferior-seq`,
after loading `emacs/seq.el`.

Specification
-------------
Types are given by the following grammar:

```
t = 0 | 1 | Int | t * t | t + t | t -> t | ∀τ.t | µτ.t
```

`*` is a commutative monoid:

```
l : ∀a.∀b.∀c.((a * (b * c)) -> ((a * b) * c))
r : ∀a.∀b.∀c.(((a * b) * c) -> (a * (b * c)))
w : ∀a.∀b.((a * b) -> (b * a))
n : ∀a.(a -> (1 * a))
m : ∀a.((1 * a) -> a)
```

`+` is a commutative monoid:

```
L : ∀a.∀b.∀c.((a + (b + c)) -> ((a + b) + c))
R : ∀a.∀b.∀c.(((a + b) + c) -> (a + (b + c)))
W : ∀a.∀b.((a + b) -> (b + a))
N : ∀a.(a -> (0 + a))
M : ∀a.((0 + a) -> a)
```

`*` distributes over `+`:

```
b : ∀a.∀b.∀c.((a * (b + c)) -> ((a * b) + (a * c)))
B : ∀a.∀b.∀c.(((a * b) + (a * c)) -> (a * (b + c)))
```

`1` is the terminal object and `0` is the initial object:

```
t : ∀a.(a -> 1)
I : ∀a.(0 -> a)
```

Values can be copied and quoted:

```
c : ∀a.(a -> (a * a))
q : ∀a.(a -> ∀b.(b -> (a * b)))
```

Sums can be merged and asserted:

```
G : ∀a.((a + a) -> a)
X : ∀a.∀b.((a + b) -> b)
```

Functions can be composed and evaluated:

```
o : ∀a.∀b.∀c.(((a -> b) * (b -> c)) -> (a -> c))
p : ∀a.∀b.∀c.∀d.(((a -> b) * (c -> d)) -> ((a * c) -> (b * d)))
S : ∀a.∀b.∀c.∀d.(((a -> b) * (c -> d)) -> ((a + c) -> (b + d)))
e : ∀a.∀b.(((a -> b) * a) -> b)
```

Integer introduction:

```
# : ∀a.(a -> (Int * a))
0 : (Int -> Int)
1 : (Int -> Int)
2 : (Int -> Int)
3 : (Int -> Int)
4 : (Int -> Int)
5 : (Int -> Int)
6 : (Int -> Int)
7 : (Int -> Int)
8 : (Int -> Int)
9 : (Int -> Int)
```

Arithmetic on integers:

```
+ : ((Int * Int) -> Int)
* : ((Int * Int) -> Int)
- : (Int -> Int)
/ : ((Int * Int) -> (1 + (Int * Int)))
% : ((Int * Int) -> (1 + (Int * Int)))
= : ((Int * Int) -> ((Int * Int) + ((Int * Int) + (Int * Int))))
```

Whitespace is identity:

```
  : ∀a.(a -> a)
```

Constructing functions:

```
     f : a -> b  g : b -> c
------------------------------
         fg : a -> c

    f : a -> c  g : b -> d
------------------------------
( f | g ) : (a * b) -> (c * d)

    f : a -> c  g : b -> d
------------------------------
{ f | g } : (a + b) -> (c + d)

          f : a -> b
------------------------------
   [f] : c -> ((a -> b) * c)
```

License
-------
Seq is available under the GNU Affero General Public License, version
3; see the LICENSE file for details.
