;; seq.el --- GNU Emacs and Seq talk to each other

;; Copyright (C) 2015 Matthew Blount

;; This file is a part of Seq.

;; Seq is free software: you can redistribute it and/or
;; modify it under the terms of the GNU Affero General
;; Public License as published by the Free Software
;; Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; Seq is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied
;; warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE. See the GNU General Public License
;; for more details.

(defvar seq-version-string "0.0.0"
  "Seq's version as a string.")

(defun seq-version ()
  "Echoes Seq's version."
  (interactive)
  (message "%s" seq-version-string))

(defun inferior-seq ()
  "Run an inferior instance of `seq` inside Emacs."
  (interactive)
  (let* ((program "iseq")
         (process-name "seq")
         (buffer-name "*seq*")
         (buffer (get-buffer-create buffer-name)))
    (when (not (comint-check-proc buffer-name))
      (make-comint-in-buffer process-name buffer-name program))
    (pop-to-buffer-same-window buffer-name)
    (inferior-seq-mode)))

(defvar seq-prompt-regexp "seq>"
  "Regexp for matching `inferior-seq` prompt.")

(defun inferior-seq-initialize ()
  (setq comint-use-prompt-regexp t))

(define-derived-mode inferior-seq-mode comint-mode "Seq"
  "Major mode for `inferior-seq`."
  nil "Seq"
  (setq comint-prompt-regexp seq-prompt-regexp)
  (setq comint-prompt-read-only t))

(add-hook 'inferior-seq-mode-hook 'inferior-seq-initialize)

(provide 'seq-mode)
