/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package main

import (
	"bufio"
	"fmt"
	"github.com/drdanmaku/seq/pkg/core/static"
	"github.com/drdanmaku/seq/pkg/core/syntax"
	"os"
)

const prompt = ">>> "
const banner = "iSeq, version 0.0.0  http://cafesofie.com/seq  :? for help"

func main() {
	stdin := bufio.NewScanner(os.Stdin)
	fmt.Println(banner)
	for {
		fmt.Print(prompt)
		stdin.Scan()
		exp, err := syntax.ReadSBC(stdin.Text())
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		tau, err := static.InferType(exp)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		fmt.Printf("%s : %s\n", exp.String(), static.String(tau))
	}
}
