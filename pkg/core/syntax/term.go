/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package syntax

import (
	"fmt"
	"log"
)

type Term interface {
	String() string
}

type Function struct {
	rune
}

type Block struct {
	child Term
}

type Sequence struct {
	fst Term
	snd Term
}

type Product struct {
	fst Term
	snd Term
}

type Sum struct {
	fst Term
	snd Term
}

func NewFunction(runes string) Term {
	var terms []Term = make([]Term, 0)
	for _, rune := range runes {
		if IsFunction(rune) == false {
			log.Panicf("Invalid rune given to NewFunction: %c", rune)
		}
		terms = append(terms, Function{rune})
	}
	return NewSequence(terms...)
}

func (tm Function) String() string {
	return fmt.Sprintf("%c", tm.rune)
}

func (tm Function) Rune() rune {
	return tm.rune
}

func NewBlock(child Term) *Block {
	return &Block{child}
}

func (tm *Block) String() string {
	return fmt.Sprintf("[%s]", tm.child.String())
}

func (tm *Block) Child() Term {
	return tm.child
}

func NewSequence(children ...Term) Term {
	if len(children) == 0 {
		return Function{' '}
	}
	if len(children) == 1 {
		return children[0]
	}
	var term Term = &Sequence{
		children[0],
		children[1],
	}
	for i := 2; i < len(children); i++ {
		term = &Sequence{term, children[i]}
	}
	return term
}

func (tm *Sequence) String() string {
	return fmt.Sprintf("%s%s", tm.fst.String(), tm.snd.String())
}

func (tm *Sequence) First() Term {
	return tm.fst
}

func (tm *Sequence) Second() Term {
	return tm.snd
}

func NewProduct(children ...Term) Term {
	if len(children) == 0 {
		return Function{' '}
	}
	if len(children) == 1 {
		return children[0]
	}
	var term Term = &Product{
		children[len(children)-2],
		children[len(children)-1],
	}
	for i := len(children) - 3; i >= 0; i-- {
		term = &Product{children[i], term}
	}
	return term
}

func (tm *Product) String() string {
	return fmt.Sprintf(
		"(%s|%s)", tm.fst.String(), tm.snd.String())
}

func (tm *Product) First() Term {
	return tm.fst
}

func (tm *Product) Second() Term {
	return tm.snd
}

func NewSum(children ...Term) Term {
	if len(children) == 0 {
		return Function{' '}
	}
	if len(children) == 1 {
		return children[0]
	}
	var term Term = &Sum{
		children[len(children)-2],
		children[len(children)-1],
	}
	for i := len(children) - 3; i >= 0; i-- {
		term = &Sum{children[i], term}
	}
	return term
}

func (tm *Sum) String() string {
	return fmt.Sprintf(
		"{%s|%s}", tm.fst.String(), tm.snd.String())
}

func (tm *Sum) First() Term {
	return tm.fst
}

func (tm *Sum) Second() Term {
	return tm.snd
}
