/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package syntax

import (
	"github.com/drdanmaku/textbox/cursor"
	"strings"
)

const (
	Functions    = `lrwnmLRWNMbBtIcGqXopSe#0123456789+*-/%= \n`
	OpenBlock    = '['
	CloseBlock   = ']'
	OpenProduct  = '('
	CloseProduct = ')'
	OpenSum      = '{'
	CloseSum     = '}'
	Separator    = '|'
	Whitespace   = " \r\n"
)

func IsFunction(ch rune) bool {
	return strings.ContainsRune(Functions, ch)
}

func IsOpenBlock(ch rune) bool {
	return ch == OpenBlock
}

func IsCloseBlock(ch rune) bool {
	return ch == CloseBlock
}

func IsOpenProduct(ch rune) bool {
	return ch == OpenProduct
}

func IsCloseProduct(ch rune) bool {
	return ch == CloseProduct
}

func IsOpenSum(ch rune) bool {
	return ch == OpenSum
}

func IsCloseSum(ch rune) bool {
	return ch == CloseSum
}

func IsOpenStructure(ch rune) bool {
	return IsOpenProduct(ch) || IsOpenSum(ch)
}

func IsCloseStructure(ch rune) bool {
	return IsCloseProduct(ch) || IsCloseSum(ch)
}

func IsDelimitStructure(ch rune) bool {
	return ch == Separator
}

func IsWhitespace(ch rune) bool {
	return strings.ContainsRune(Whitespace, ch)
}

type mkTerm func(...Term) Term

func ReadSBC(source string) (Term, error) {
	var terms []Term = make([]Term, 0)
	stream := cursor.New(source)
	for stream.Ok() {
		if stream.Accept(IsFunction) {
			fun := NewFunction(string(stream.Current()))
			terms = append(terms, fun)
			stream.Advance()
		} else if stream.Accept(IsOpenBlock) {
			block, err := readSBCBlock(stream)
			if err != nil {
				return nil, err
			}
			terms = append(terms, block)
		} else if stream.Accept(IsOpenProduct) {
			product, err := readSBCProduct(stream)
			if err != nil {
				return nil, err
			}
			terms = append(terms, product)
		} else if stream.Accept(IsOpenSum) {
			sum, err := readSBCSum(stream)
			if err != nil {
				return nil, err
			}
			terms = append(terms, sum)
		} else if stream.Accept(IsWhitespace) {
			stream.AdvanceWhile(IsWhitespace)
		} else {
			return nil, UnknownRune{stream.Source(), stream.Point()}
		}
	}
	return NewSequence(terms...), nil
}

func readSBCBlock(stream *cursor.Cursor) (Term, error) {
	stream.Advance()
	if stream.End() {
		return nil, UnbalancedBlock{stream.Source(), stream.Point()}
	}
	depth := 0
	stream.Save()
loop:
	for stream.Ok() {
		if stream.Accept(IsOpenBlock) {
			depth++
		} else if stream.Accept(IsCloseBlock) {
			if depth == 0 {
				break loop
			}
			depth--
		}
		stream.Advance()
	}
	if stream.End() {
		return nil, UnbalancedBlock{stream.Source(), stream.Point()}
	}
	region := stream.Region()
	stream.Advance()
	body, err := ReadSBC(region)
	if err != nil {
		return nil, err
	}
	return NewBlock(body), nil
}

func readSBCProduct(stream *cursor.Cursor) (Term, error) {
	return readSBCDelimited(stream, []rune{'('})
}

func readSBCSum(stream *cursor.Cursor) (Term, error) {
	return readSBCDelimited(stream, []rune{'{'})
}

func match(ch rune) rune {
	switch ch {
	case '(':
		return ')'
	case ')':
		return '('
	case '{':
		return '}'
	case '}':
		return '{'
	case '[':
		return ']'
	case ']':
		return '['
	}
	panic("invalid rune given to match")
}

func readSBCDelimited(
	stream *cursor.Cursor, stack []rune) (Term, error) {
	if len(stack) != 1 {
		panic("Invalid stack given to reader")
	}
	stream.Advance()
	if stream.End() {
		return nil, UnbalancedBlock{stream.Source(), stream.Point()}
	}
	strings := make([]string, 0)
	stream.Save()
loop:
	for stream.Ok() {
		if stream.Accept(IsOpenStructure) {
			stack = append(stack, stream.Current())
			stream.Advance()
		} else if stream.Accept(IsCloseStructure) {
			rune := stream.Current()
			pair := match(rune)
			if pair != stack[len(stack)-1] {
				return nil, UnbalancedBlock{stream.Source(), stream.Point()}
			}
			if len(stack) == 1 {
				break loop
			}
			stack = stack[:len(stack)-1]
			stream.Advance()
		} else if stream.Accept(IsDelimitStructure) && len(stack) == 1 {
			strings = append(strings, stream.Region())
			stream.Advance()
			stream.Save()
		} else {
			stream.Advance()
		}
	}
	if stream.End() {
		return nil, UnbalancedBlock{stream.Source(), stream.Point()}
	}
	strings = append(strings, stream.Region())
	stream.Advance()
	var terms []Term = make([]Term, 0)
	for _, string := range strings {
		term, err := ReadSBC(string)
		if err != nil {
			return nil, err
		}
		terms = append(terms, term)
	}
	switch stack[0] {
	case OpenProduct:
		return NewProduct(terms...), nil
	case OpenSum:
		return NewSum(terms...), nil
	}
	panic("Invalid brace given to reader")
}
