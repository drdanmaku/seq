/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package syntax

import (
	"fmt"
)

type UnbalancedBlock struct {
	source string
	index  int
}

type UnknownRune struct {
	source string
	index  int
}

func (err UnbalancedBlock) Error() string {
	return fmt.Sprintf(
		"Unbalanced block in\n\n%s\n\nat position %d",
		err.source, err.index)
}

func (err UnknownRune) Error() string {
	return fmt.Sprintf(
		"Unknown rune in\n\n%s\n\nat position %d",
		err.source, err.index)
}
