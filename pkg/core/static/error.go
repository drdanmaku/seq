/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import (
	"fmt"
)

type Undefined struct {
	key lvar
}

type Redefined struct {
	key     lvar
	attempt Type
	actual  Type
}

type NoUnifier struct {
	fst Type
	snd Type
}

func (err Undefined) Error() string {
	return fmt.Sprintf(
		"Undefined variable: %s", String(err.key))
}

func (err Redefined) Error() string {
	return fmt.Sprintf(
		"Tried to redefine %s as %s, but it's already defined as %s",
		String(err.key),
		String(err.attempt),
		String(err.actual))
}

func (err NoUnifier) Error() string {
	return fmt.Sprintf(
		"Cannot unify %s and %s",
		String(err.fst),
		String(err.snd))
}
