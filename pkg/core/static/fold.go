/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

func fold(ty Type, u lvar) Type {
	return NewFixpoint(ty.fold(u.name, 0))
}

func unfold(ty *Fixpoint) Type {
	return ty.child.unfold(0, ty)
}

func (ty Void) fold(find uint64, replace uint64) Type {
	return ty
}

func (ty Void) unfold(find uint64, replace *Fixpoint) Type {
	return ty
}

func (ty Unit) fold(find uint64, replace uint64) Type {
	return ty
}

func (ty Unit) unfold(find uint64, replace *Fixpoint) Type {
	return ty
}

func (ty Int) fold(find uint64, replace uint64) Type {
	return ty
}

func (ty Int) unfold(find uint64, replace *Fixpoint) Type {
	return ty
}

func (ty *Product) fold(find uint64, replace uint64) Type {
	return NewProduct(
		ty.fst.fold(find, replace),
		ty.snd.fold(find, replace))
}

func (ty *Product) unfold(find uint64, replace *Fixpoint) Type {
	return NewProduct(
		ty.fst.unfold(find, replace),
		ty.snd.unfold(find, replace))
}

func (ty *Sum) fold(find uint64, replace uint64) Type {
	return NewSum(
		ty.fst.fold(find, replace),
		ty.snd.fold(find, replace))
}

func (ty *Sum) unfold(find uint64, replace *Fixpoint) Type {
	return NewSum(
		ty.fst.unfold(find, replace),
		ty.snd.unfold(find, replace))
}

func (ty *Arrow) fold(find uint64, replace uint64) Type {
	return NewArrow(
		ty.fst.fold(find, replace),
		ty.snd.fold(find, replace))
}

func (ty *Arrow) unfold(find uint64, replace *Fixpoint) Type {
	return NewArrow(
		ty.fst.unfold(find, replace),
		ty.snd.unfold(find, replace))
}

func (ty *Universal) fold(find uint64, replace uint64) Type {
	return NewUniversal(ty.child.fold(find, replace+1))
}

func (ty *Universal) unfold(find uint64, replace *Fixpoint) Type {
	return NewUniversal(ty.child.unfold(find+1, replace))
}

func (ty *Fixpoint) fold(find uint64, replace uint64) Type {
	return NewFixpoint(ty.child.fold(find, replace+1))
}

func (ty *Fixpoint) unfold(find uint64, replace *Fixpoint) Type {
	return NewFixpoint(ty.child.unfold(find+1, replace))
}

func (ty TVar) fold(find uint64, replace uint64) Type {
	return ty
}

func (ty TVar) unfold(find uint64, replace *Fixpoint) Type {
	if ty.name == find {
		return replace
	}
	return ty
}

func (ty lvar) fold(find uint64, replace uint64) Type {
	if ty.name == find {
		return NewTVar(replace)
	}
	return ty
}

func (ty lvar) unfold(find uint64, replace *Fixpoint) Type {
	return ty
}
