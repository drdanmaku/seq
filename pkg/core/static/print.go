/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import (
	"fmt"
	"github.com/drdanmaku/textbox/buffer"
)

func String(ty Type) string {
	pr := newPrinter()
	ty.print(pr)
	return pr.buf.String()
}

type printer struct {
	vars map[uint64]rune
	next int32
	fix  []rune
	buf  *buffer.Buffer
}

func newPrinter() *printer {
	return &printer{
		vars: make(map[uint64]rune),
		next: int32('a'),
		fix:  make([]rune, 0),
		buf:  buffer.New(64),
	}
}

func (pr *printer) fresh() rune {
	fresh := rune(pr.next)
	pr.next++
	return fresh
}

func (pr *printer) getLVar(ty lvar) rune {
	rune, ok := pr.vars[ty.name]
	if ok == false {
		rune = pr.fresh()
		pr.vars[ty.name] = rune
	}
	return rune
}

func (pr *printer) pushTVar() rune {
	rune := pr.fresh()
	pr.fix = append(pr.fix, rune)
	return rune
}

func (pr *printer) popTVar() {
	pr.fix = pr.fix[:len(pr.fix)-1]
}

func (pr *printer) getTVar(rec TVar) rune {
	return pr.fix[len(pr.fix)-1-int(rec.name)]
}

func (ty Void) print(pr *printer) {
	pr.buf.InsertRune('0')
}

func (ty Unit) print(pr *printer) {
	pr.buf.InsertRune('1')
}

func (ty Int) print(pr *printer) {
	pr.buf.InsertString("Int")
}

func (ty *Product) print(pr *printer) {
	pr.buf.InsertRune('(')
	ty.fst.print(pr)
	pr.buf.InsertString(" * ")
	ty.snd.print(pr)
	pr.buf.InsertRune(')')
}

func (ty *Sum) print(pr *printer) {
	pr.buf.InsertRune('(')
	ty.fst.print(pr)
	pr.buf.InsertString(" + ")
	ty.snd.print(pr)
	pr.buf.InsertRune(')')
}

func (ty *Arrow) print(pr *printer) {
	pr.buf.InsertRune('(')
	ty.fst.print(pr)
	pr.buf.InsertString(" -> ")
	ty.snd.print(pr)
	pr.buf.InsertRune(')')
}

func (ty *Universal) print(pr *printer) {
	rune := pr.pushTVar()
	pr.buf.InsertString(fmt.Sprintf("∀%c.", rune))
	ty.child.print(pr)
	pr.popTVar()
}

func (ty *Fixpoint) print(pr *printer) {
	rune := pr.pushTVar()
	pr.buf.InsertString(fmt.Sprintf("µ%c.", rune))
	ty.child.print(pr)
	pr.popTVar()
}

func (ty TVar) print(pr *printer) {
	rune := pr.getTVar(ty)
	pr.buf.InsertRune(rune)
}

func (ty lvar) print(pr *printer) {
	rune := pr.getLVar(ty)
	pr.buf.InsertRune(rune)
}
