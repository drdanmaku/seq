/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import (
	"github.com/drdanmaku/seq/pkg/core/syntax"
	"log"
)

// InferType maps an expression to its type. See the documentation for
// details on the Seq type system.
func InferType(expr syntax.Term) (Type, error) {
	subst := newSubst()
	ty, err := inferType(expr, subst)
	if err != nil {
		return nil, err
	}
	return generalize(ty, subst), nil
}

func inferType(expr syntax.Term, subst *subst) (Type, error) {
	switch expr := expr.(type) {
	case syntax.Function:
		switch expr.Rune() {
		// NOTE: Technically the primitive operators have several
		// layers of universal quantification around them, but
		// I think we can get away without putting them in
		// immediately because blocks and the outer call to
		// InferType will generalize for us.
		case 'l':
			// l : A * (B * C) -> (A * B) * C
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewProduct(a, NewProduct(b, c)),
				NewProduct(NewProduct(a, b), c)), nil
		case 'r':
			// r : (A * B) * C -> A * (B * C)
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewProduct(NewProduct(a, b), c),
				NewProduct(a, NewProduct(b, c))), nil
		case 'w':
			// w : (A * B) -> (B * A)
			a := newLVar()
			b := newLVar()
			return NewArrow(
				NewProduct(a, b), NewProduct(b, a)), nil
		case 'n':
			// n : A -> (1 * A)
			a := newLVar()
			return NewArrow(a, NewProduct(UnitType, a)), nil
		case 'm':
			// m : (1 * A) -> A
			a := newLVar()
			return NewArrow(NewProduct(UnitType, a), a), nil
		case 'L':
			// L : (a + (b + c)) -> ((a + b) + c)
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewSum(a, NewSum(b, c)),
				NewSum(NewSum(a, b), c)), nil
		case 'R':
			// R : ((a + b) + c) -> (a + (b + c))
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewSum(NewSum(a, b), c),
				NewSum(a, NewSum(b, c))), nil
		case 'W':
			// W : (a + b) -> (b + a)
			a := newLVar()
			b := newLVar()
			return NewArrow(NewSum(a, b), NewSum(b, a)), nil
		case 'N':
			// N : a -> (0 + a)
			a := newLVar()
			return NewArrow(a, NewSum(VoidType, a)), nil
		case 'M':
			// M : (0 + a) -> a
			a := newLVar()
			return NewArrow(NewSum(VoidType, a), a), nil
		case 'b':
			// b : (a * (b + c)) -> ((a * b) + (a * c))
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewProduct(a, NewSum(b, c)),
				NewSum(NewProduct(a, b), NewProduct(a, c))), nil
		case 'B':
			// B : ((a * b) + (a * c)) -> (a * (b + c))
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewSum(NewProduct(a, b), NewProduct(a, c)),
				NewProduct(a, NewSum(b, c))), nil
		case 't':
			// t : a -> 1
			a := newLVar()
			return NewArrow(a, UnitType), nil
		case 'I':
			// I : 0 -> a
			a := newLVar()
			return NewArrow(VoidType, a), nil
		case 'c':
			// c : a -> (a * a)
			a := newLVar()
			return NewArrow(a, NewProduct(a, a)), nil
		case 'G':
			// G : (a + a) -> a
			a := newLVar()
			return NewArrow(NewSum(a, a), a), nil
		case 'q':
			// q : a -> (b -> (a * b))
			a := newLVar()
			b := NewTVar(0)
			return NewArrow(a, NewUniversal(NewArrow(b, NewProduct(a, b)))), nil
		case 'X':
			// X : (a + b) -> b
			a := newLVar()
			b := newLVar()
			return NewArrow(NewSum(a, b), b), nil
		case 'o':
			// o : ((a -> b) * (b -> c)) -> (a -> c)
			a := newLVar()
			b := newLVar()
			c := newLVar()
			return NewArrow(
				NewProduct(NewArrow(a, b), NewArrow(b, c)),
				NewArrow(a, c)), nil
		case 'p':
			// p : ((a -> c) * (b -> d)) -> ((a * b) -> (c * d))
			a := newLVar()
			b := newLVar()
			c := newLVar()
			d := newLVar()
			return NewArrow(
				NewProduct(NewArrow(a, c), NewArrow(b, d)),
				NewArrow(NewProduct(a, b), NewProduct(c, d))), nil
		case 'S':
			// S : ((a -> c) * (b -> d)) -> ((a + b) -> (c + d))
			a := newLVar()
			b := newLVar()
			c := newLVar()
			d := newLVar()
			return NewArrow(
				NewProduct(NewArrow(a, c), NewArrow(b, d)),
				NewArrow(NewSum(a, b), NewSum(c, d))), nil
		case 'e':
			// e : ((a -> b) * a) -> b
			a := newLVar()
			b := newLVar()
			return NewArrow(NewProduct(NewArrow(a, b), a), b), nil
		case '#':
			// # : a -> (Int * a)
			a := newLVar()
			return NewArrow(a, NewProduct(IntType, a)), nil
		case '0':
			fallthrough
		case '1':
			fallthrough
		case '2':
			fallthrough
		case '3':
			fallthrough
		case '4':
			fallthrough
		case '5':
			fallthrough
		case '6':
			fallthrough
		case '7':
			fallthrough
		case '8':
			fallthrough
		case '9':
			// 0..9 : Int -> Int
			return NewArrow(IntType, IntType), nil
		case '+':
			// + : (Int * Int) -> Int
			return NewArrow(NewProduct(IntType, IntType), IntType), nil
		case '*':
			// * : (Int * Int) -> Int
			return NewArrow(NewProduct(IntType, IntType), IntType), nil
		case '-':
			// - : Int -> Int
			return NewArrow(IntType, IntType), nil
		case '/':
			// / : (Int * Int) -> (1 + (Int * Int))
			return NewArrow(
				NewProduct(IntType, IntType),
				NewSum(UnitType, NewProduct(IntType, IntType))), nil
		case '%':
			// % : (Int * Int) -> (1 + (Int * Int))
			return NewArrow(
				NewProduct(IntType, IntType),
				NewSum(UnitType, NewProduct(IntType, IntType))), nil
		case '=':
			// = : (Int * Int) -> ((Int * Int) + (Int * Int) + (Int * Int))
			pair := NewProduct(IntType, IntType)
			return NewArrow(pair, NewSum(pair, NewSum(pair, pair))), nil
		case ' ':
			// whitespace : A -> A
			fallthrough
		case '\r':
			fallthrough
		case '\n':
			// whitespace : A -> A
			a := newLVar()
			return NewArrow(a, a), nil
		default:
			log.Panicf("unknown function in inferType: %c", expr.Rune())
		}
	case *syntax.Block:
		//         f : A -> B
		// --------------------------
		//    [f] : C -> ((A -> B) * C)
		body, err := InferType(expr.Child())
		if err != nil {
			return nil, err
		}
		c := newLVar()
		return NewArrow(c, NewProduct(body, c)), nil
	case *syntax.Sequence:
		// f : A -> B    g : B -> C
		// ------------------------
		//       fg : A -> C
		lhs, err := inferArrow(expr.First(), subst)
		if err != nil {
			return nil, err
		}
		rhs, err := inferArrow(expr.Second(), subst)
		if err != nil {
			return nil, err
		}
		if err := unify(lhs.snd, rhs.fst, subst); err != nil {
			return nil, err
		}
		return NewArrow(lhs.fst, rhs.snd).apply(subst), nil
	case *syntax.Product:
		//		 f : A -> C  g : B -> D
		// -----------------------------
		// ( f | g ) : (A * B) -> (C * D)
		lhs, err := inferArrow(expr.First(), subst)
		if err != nil {
			return nil, err
		}
		rhs, err := inferArrow(expr.Second(), subst)
		if err != nil {
			return nil, err
		}
		return NewArrow(
			NewProduct(lhs.fst, rhs.fst),
			NewProduct(lhs.snd, rhs.snd)), nil
	case *syntax.Sum:
		//		 f : A -> C  g : B -> D
		// -----------------------------
		// { f | g } : (A + B) -> (C + D)
		lhs, err := inferArrow(expr.First(), subst)
		if err != nil {
			return nil, err
		}
		rhs, err := inferArrow(expr.Second(), subst)
		if err != nil {
			return nil, err
		}
		return NewArrow(
			NewSum(lhs.fst, rhs.fst),
			NewSum(lhs.snd, rhs.snd)), nil
	}
	panic("Invalid term in inferType")
}

// inferArrow infers a type that must be an arrow.
func inferArrow(expr syntax.Term, subst *subst) (*Arrow, error) {
	ty, err := inferType(expr, subst)
	if err != nil {
		return nil, err
	}
	arr := NewArrow(newLVar(), newLVar())
	if err := unify(ty, arr, subst); err != nil {
		return nil, err
	}
	return arr, nil
}
