/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import ()

type Type interface {
	// print.go
	print(*printer)
	// unify.go
	unify(Type, *subst) error
	apply(*subst) Type
	occurs(lvar) bool
	unboundLVars(*subst, map[uint64]bool, *[]uint64)
	// generalize.go
	generalize(uint64, uint64) Type
	instantiate(uint64, lvar) Type
	// fold.go
	fold(uint64, uint64) Type
	unfold(uint64, *Fixpoint) Type
}

type Void struct{}

var VoidType = Void{}

type Unit struct{}

var UnitType = Unit{}

type Int struct{}

var IntType = Int{}

type Product struct {
	fst Type
	snd Type
}

func NewProduct(fst, snd Type) *Product {
	return &Product{fst, snd}
}

type Sum struct {
	fst Type
	snd Type
}

func NewSum(fst, snd Type) *Sum {
	return &Sum{fst, snd}
}

type Arrow struct {
	fst Type
	snd Type
}

func NewArrow(fst, snd Type) *Arrow {
	return &Arrow{fst, snd}
}

type Universal struct {
	child Type
}

func NewUniversal(child Type) *Universal {
	return &Universal{child}
}

type Fixpoint struct {
	child Type
}

func NewFixpoint(child Type) *Fixpoint {
	return &Fixpoint{child}
}

type TVar struct {
	name uint64
}

func NewTVar(name uint64) TVar {
	return TVar{name}
}

var fresh chan uint64

func init() {
	fresh = make(chan uint64, 10)
	go func() {
		var guid uint64
		for {
			fresh <- guid
			guid++
		}
	}()
}

type lvar struct {
	name uint64
}

func newLVar() lvar {
	return lvar{<-fresh}
}
