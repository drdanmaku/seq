/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

func unify(fst, snd Type, subst *subst) error {
	return fst.apply(subst).unify(snd.apply(subst), subst)
}

func (ty Void) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case Void:
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty Void) apply(subst *subst) Type {
	return ty
}

func (ty Void) occurs(u lvar) bool {
	return false
}

func (ty Unit) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case Unit:
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty Unit) apply(subst *subst) Type {
	return ty
}

func (ty Unit) occurs(u lvar) bool {
	return false
}

func (ty Int) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case Int:
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty Int) apply(subst *subst) Type {
	return ty
}

func (ty Int) occurs(u lvar) bool {
	return false
}

func (ty *Product) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case *Product:
		if err := ty.fst.unify(u.fst, subst); err != nil {
			return err
		}
		if err := unify(ty.snd, u.snd, subst); err != nil {
			return err
		}
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty *Product) apply(subst *subst) Type {
	return NewProduct(ty.fst.apply(subst), ty.snd.apply(subst))
}

func (ty *Product) occurs(u lvar) bool {
	return ty.fst.occurs(u) || ty.snd.occurs(u)
}

func (ty *Sum) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case *Sum:
		if err := ty.fst.unify(u.fst, subst); err != nil {
			return err
		}
		if err := unify(ty.snd, u.snd, subst); err != nil {
			return err
		}
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty *Sum) apply(subst *subst) Type {
	return NewSum(ty.fst.apply(subst), ty.snd.apply(subst))
}

func (ty *Sum) occurs(u lvar) bool {
	return ty.fst.occurs(u) || ty.snd.occurs(u)
}

func (ty *Arrow) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case *Arrow:
		if err := ty.fst.unify(u.fst, subst); err != nil {
			return err
		}
		if err := unify(ty.snd, u.snd, subst); err != nil {
			return err
		}
		return nil
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty *Arrow) apply(subst *subst) Type {
	return NewArrow(ty.fst.apply(subst), ty.snd.apply(subst))
}

func (ty *Arrow) occurs(u lvar) bool {
	return ty.fst.occurs(u) || ty.snd.occurs(u)
}

func (ty *Universal) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case *Universal:
		return ty.child.unify(u.child, subst)
	case lvar:
		return u.unify(ty, subst)
	default:
		return instantiate(ty).unify(u, subst)
	}
}

func (ty *Universal) apply(subst *subst) Type {
	return NewUniversal(ty.child.apply(subst))
}

func (ty *Universal) occurs(u lvar) bool {
	return ty.child.occurs(u)
}

func (ty *Fixpoint) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case *Fixpoint:
		return ty.child.unify(u.child, subst)
	case lvar:
		return u.unify(ty, subst)
	default:
		return unfold(ty).unify(u, subst)
	}
}

func (ty *Fixpoint) apply(subst *subst) Type {
	return NewFixpoint(ty.child.apply(subst))
}

func (ty *Fixpoint) occurs(u lvar) bool {
	return ty.child.occurs(u)
}

func (ty TVar) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case TVar:
		if ty.name == u.name {
			return nil
		}
		return NoUnifier{ty, u}
	case lvar:
		return u.unify(ty, subst)
	case *Universal:
		return ty.unify(instantiate(u), subst)
	case *Fixpoint:
		return ty.unify(unfold(u), subst)
	default:
		return NoUnifier{ty, u}
	}
}

func (ty TVar) apply(subst *subst) Type {
	return ty
}

func (ty TVar) occurs(u lvar) bool {
	return false
}

func (ty lvar) unify(u Type, subst *subst) error {
	switch u := u.(type) {
	case Void:
		return subst.set(ty, u)
	case Unit:
		return subst.set(ty, u)
	case Int:
		return subst.set(ty, u)
	case TVar:
		return subst.set(ty, u)
	case lvar:
		if u.occurs(ty) {
			panic("LVar unified with itself, should this ever happen?")
		}
		return subst.set(ty, u)
	case *Product:
		if u.occurs(ty) {
			return subst.set(ty, fold(u, ty))
		}
		return subst.set(ty, u)
	case *Sum:
		if u.occurs(ty) {
			return subst.set(ty, fold(u, ty))
		}
		return subst.set(ty, u)
	case *Arrow:
		if u.occurs(ty) {
			return subst.set(ty, fold(u, ty))
		}
		return subst.set(ty, u)
	case *Universal:
		if u.occurs(ty) {
			return subst.set(ty, fold(u, ty))
		}
		return subst.set(ty, u)
	case *Fixpoint:
		if u.occurs(ty) {
			return subst.set(ty, fold(u, ty))
		}
		return subst.set(ty, u)
	default:
		panic("unreachable")
	}
}

func (ty lvar) occurs(u lvar) bool {
	return ty.name == u.name
}

func (ty lvar) apply(subst *subst) Type {
	u, err := subst.get(ty)
	if err != nil {
		return ty
	}
	return u.apply(subst)
}
