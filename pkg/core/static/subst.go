/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import ()

type subst struct {
	bindings map[uint64]Type
}

func newSubst() *subst {
	return &subst{
		bindings: make(map[uint64]Type),
	}
}

func (subst *subst) set(key lvar, value Type) error {
	current, ok := subst.bindings[key.name]
	if ok {
		return Redefined{key, value, current}
	}
	subst.bindings[key.name] = value
	return nil
}

func (subst *subst) get(key lvar) (Type, error) {
	value, ok := subst.bindings[key.name]
	if ok == false {
		return nil, Undefined{key}
	}
	return value, nil
}
