/*
Copyright (C) 2015 Matthew Blount

This file is a part of Seq.

Seq is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Seq is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.
*/

package static

import ()

// Generalize replaces the unbound logic variables in
// a type with type variables pointing to an enclosing
// universal quantifier.
func generalize(ty Type, subst *subst) Type {
	ty = ty.apply(subst)
	seen := make(map[uint64]bool)
	unbound := make([]uint64, 0)
	ty.unboundLVars(subst, seen, &unbound)
	for i := len(unbound) - 1; i >= 0; i-- {
		ty = NewUniversal(ty.generalize(unbound[i], 0))
	}
	return ty
}

// Instantiate replaces the type variables pointing to
// the given universal quantifier with fresh logic
// variables.
func instantiate(ty *Universal) Type {
	return ty.child.instantiate(0, newLVar())
}

func (ty Void) generalize(find, replace uint64) Type {
	return ty
}

func (ty Void) instantiate(find uint64, replace lvar) Type {
	return ty
}

func (ty Void) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	//
}

func (ty Unit) generalize(find, replace uint64) Type {
	return ty
}

func (ty Unit) instantiate(find uint64, replace lvar) Type {
	return ty
}

func (ty Unit) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	//
}

func (ty Int) generalize(find, replace uint64) Type {
	return ty
}

func (ty Int) instantiate(find uint64, replace lvar) Type {
	return ty
}

func (ty Int) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	//
}

func (ty *Product) generalize(find, replace uint64) Type {
	return NewProduct(
		ty.fst.generalize(find, replace),
		ty.snd.generalize(find, replace))
}

func (ty *Product) instantiate(find uint64, replace lvar) Type {
	return NewProduct(
		ty.fst.instantiate(find, replace),
		ty.snd.instantiate(find, replace))
}

func (ty *Product) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	ty.fst.unboundLVars(subst, set, out)
	ty.snd.unboundLVars(subst, set, out)
}

func (ty *Sum) generalize(find, replace uint64) Type {
	return NewSum(
		ty.fst.generalize(find, replace),
		ty.snd.generalize(find, replace))
}

func (ty *Sum) instantiate(find uint64, replace lvar) Type {
	return NewSum(
		ty.fst.instantiate(find, replace),
		ty.snd.instantiate(find, replace))
}

func (ty *Sum) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	ty.fst.unboundLVars(subst, set, out)
	ty.snd.unboundLVars(subst, set, out)
}

func (ty *Arrow) generalize(find, replace uint64) Type {
	return NewArrow(
		ty.fst.generalize(find, replace),
		ty.snd.generalize(find, replace))
}

func (ty *Arrow) instantiate(find uint64, replace lvar) Type {
	return NewArrow(
		ty.fst.instantiate(find, replace),
		ty.snd.instantiate(find, replace))
}

func (ty *Arrow) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	ty.fst.unboundLVars(subst, set, out)
	ty.snd.unboundLVars(subst, set, out)
}

func (ty *Universal) generalize(find, replace uint64) Type {
	return NewUniversal(ty.child.generalize(find, replace+1))
}

func (ty *Universal) instantiate(find uint64, replace lvar) Type {
	return NewUniversal(ty.child.instantiate(find+1, replace))
}

func (ty *Universal) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	ty.child.unboundLVars(subst, set, out)
}

func (ty *Fixpoint) generalize(find, replace uint64) Type {
	return NewFixpoint(ty.child.generalize(find, replace+1))
}

func (ty *Fixpoint) instantiate(find uint64, replace lvar) Type {
	return NewFixpoint(ty.child.instantiate(find+1, replace))
}

func (ty *Fixpoint) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	ty.child.unboundLVars(subst, set, out)
}

func (ty TVar) generalize(find, replace uint64) Type {
	return ty
}

func (ty TVar) instantiate(find uint64, replace lvar) Type {
	if ty.name == find {
		return replace
	}
	return ty
}

func (ty TVar) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	//
}

func (ty lvar) generalize(find, replace uint64) Type {
	if ty.name == find {
		return NewTVar(replace)
	}
	return ty
}

func (ty lvar) instantiate(find uint64, replace lvar) Type {
	return ty
}

func (ty lvar) unboundLVars(
	subst *subst, set map[uint64]bool, out *[]uint64) {
	_, err := subst.get(ty)
	if err != nil {
		_, ok := set[ty.name]
		if ok == false {
			set[ty.name] = true
			*out = append(*out, ty.name)
		}
	}
}
